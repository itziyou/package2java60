import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class Run {

	/**
	 * 目标目录
	 */
	public static String targetDir = "D:\\workspace\\crmone\\crmone\\src\\com\\weixin";

	/**
	 * 目标编码
	 */
	public static String targetEncode = "utf-8";

	public static String srcEncode = "gb2312";
	
	//	public static String targetRange = ".html;.htm;.js;";// .txt;

	public static String targetRange = ".java;";// .txt;
	
	//生成目录
	public static String result_file = "C:\\Users\\sunfish\\Desktop\\master\\61.java";//

	/**
	 * @param args
	 * @throws IOException
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	public static void main(String[] args) throws FileNotFoundException,
			UnsupportedEncodingException, IOException {
		Convert.convert();
	}

}
